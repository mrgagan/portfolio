from django.db import models

# Create your models here.
class Author(models.Model):
    first_name = models.CharField(max_length=150, blank=False)
    last_name = models.CharField(max_length=150, blank=False)
    email = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

class Blog(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(blank=True)
    image = models.FileField(upload_to="img", blank=True)
    published_date = models.DateTimeField(auto_now=True)
    publish = models.BooleanField(default=False)

    def __str__(self):
        return self.title

