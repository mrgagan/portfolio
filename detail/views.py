from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from detail.models import Blog


def index(request):
    return HttpResponse("hello this is detail index page")

def blogList(request):
    # need html template to show the bloglist
    # access the data from the model
    # pass the data to the bloglist html
    if request.method == "GET":
        data = Blog.objects.filter(author_id=1)
        print(len(data))
        return render(request, 'detail/bloglist.html', {'bloglist':data})
    else:
        return HttpResponse("Only supports get request")
